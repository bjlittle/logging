"""Testing for `daily_usage_by_version`."""

import os
import unittest

from daily_usage_by_version import PythonUsageDaily


def check_list_items_same(l1, l2):
    """
    Check that two lists contain the same items regardless of order.

    Args:

        * l1: first list to check
        * l2: second list to check

    Returns:
        A boolean describing whether the two lists have the same contents.

    """
    result = set(l1) - set(l2)
    return not len(result)


class TestResults(unittest.TestCase):
    # Check that instantiating `PythonUsageDaily` loads the correct
    # Python usage input files and popluates the appropriate
    # instance attributes.
    # Here when testing, the file that should be loaded is `00100100.txt`, and
    # `self.usages.filenames`, `self.usages.results` and
    # `self.usages.all_versions` should all be populated.
    def setUp(self):
        self.cwd = os.path.dirname(os.path.abspath(__file__))
        self.usages = PythonUsageDaily(self.cwd, 'does_not_exist.txt',
                                       all_files=True)

    def test_input_filenames(self):
        expected = [os.path.join(self.cwd, '00100100.txt')]
        result = self.usages.filenames
        self.assertEqual(expected, result)

    def test_results_populated(self):
        # We've only got a single test file.
        expected_len = 1
        result = len(self.usages.results)
        self.assertEqual(expected_len, result)

    def test_result_versions(self):
        # There are three rows in the test file, each describing one version.
        expected_versions = ['2.7.11', '2.7.12', '2.7.6']
        result = self.usages.all_versions
        test_result = check_list_items_same(expected_versions, result)
        self.assertTrue(test_result)


class TestVersions__no_existing_file(unittest.TestCase):
    def setUp(self):
        # In this case the versions used should be those in the input files,
        # that is `00100100.txt`.
        cwd = os.path.dirname(os.path.abspath(__file__))
        self.usages = PythonUsageDaily(cwd, 'does_not_exist.txt',
                                       all_files=True)
        self.usages.get_results()
        self.expected_versions = ['2.7.11', '2.7.12', '2.7.6']

    def test_existing_versions(self):
        self.assertFalse(self.usages.existing_versions)

    def test_month_versions(self):
        test_result = check_list_items_same(self.expected_versions,
                                            self.usages.month_versions)
        self.assertTrue(test_result)

    def test_all_versions(self):
        test_result = check_list_items_same(self.expected_versions,
                                            self.usages.all_versions)
        self.assertTrue(test_result)


class TestVersions__no_new_versions(unittest.TestCase):
    # Check that if the Python versions in the save file
    # (`no_new_versions.txt`) and the monthly processing file (`00100100.txt`)
    # are the same, then the versions attributes are set to these common
    # Python versions.
    def setUp(self):
        cwd = os.path.dirname(os.path.abspath(__file__))
        self.usages = PythonUsageDaily(cwd, 'no_new_versions.txt',
                                       all_files=True)
        self.usages.get_results()
        self.expected_versions = ['2.7.11', '2.7.12', '2.7.6']

    def test_existing_versions(self):
        # The test file contains the same versions as the expected versions.
        test_result = check_list_items_same(self.usages.existing_versions,
                                            self.expected_versions)
        self.assertTrue(test_result)

    def test_month_versions(self):
        test_result = check_list_items_same(self.expected_versions,
                                            self.usages.month_versions)
        self.assertTrue(test_result)

    def test_all_versions(self):
        test_result = check_list_items_same(self.expected_versions,
                                            self.usages.all_versions)
        self.assertTrue(test_result)


class TestVersions__new_versions_introduced(unittest.TestCase):
    def setUp(self):
        cwd = os.path.dirname(os.path.abspath(__file__))
        self.usages = PythonUsageDaily(cwd, 'new_versions.txt',
                                       all_files=True)
        self.usages.get_results()

    def test_existing_versions(self):
        # Order of versions is important in this test.
        # These versions come from `new_versions.txt`.
        expected_versions = ['2.7.6', '2.7.12', '3.6.5']
        result = self.usages.existing_versions
        self.assertEqual(expected_versions, result)

    def test_month_versions(self):
        # The versions from this "month" are the versions in the test input
        # file `00100100.txt`.
        expected_versions = ['2.7.11', '2.7.12', '2.7.6']
        test_result = check_list_items_same(expected_versions,
                                            self.usages.month_versions)
        self.assertTrue(test_result)

    def test_all_versions(self):
        # Order of versions is important in this test. The newly-introduced
        # version needs to be appended to the end of the list of existing
        # versions.
        # The new version is `2.7.11`, which must be appended to the existing
        # versions.
        expected_versions = ['2.7.6', '2.7.12', '3.6.5', '2.7.11']
        self.assertEqual(expected_versions, self.usages.all_versions)


if __name__ == '__main__':
    unittest.main()
