"""
A simple tornado web server for logging invocations of Pythons from the
SciTools stacks.

"""

import datetime
from logging.handlers import DEFAULT_HTTP_LOGGING_PORT
import os

import tornado.ioloop
import tornado.web


class Writer(object):
    # Log files stored on local disk as they are constantly being appended to.
    filepath = '/data/local/avd/logs'

    def __init__(self):
        """Construct a Writer to write received log messages to a file."""
        self._fname = None
        self._filename = None
        self._check_fname()

    def __call__(self, dt, body):
        """
        Write the received logging item to file.

        Args:

            * dt: The unix time reference of when the log was made.

            * body: the message of the logging item.

        """
        self.dt = dt
        self.body = body

        self._check_fname()
        self.datetime = self._make_datetime_from_unixtime()
        self.write()

    @property
    def fname(self):
        return self._fname

    @fname.setter
    def fname(self, value):
        self._fname = value

    @property
    def filename(self):
        filename = '{}.txt'.format(self.fname)
        self.filename = os.path.join(self.filepath, filename)
        return self._filename

    @filename.setter
    def filename(self, value):
        self._filename = value

    def _check_fname(self):
        now = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d')
        if self.fname != now:
            self.fname = now

    def _make_datetime_from_unixtime(self):
        unix_time = float(self.dt)
        unix_dt = datetime.datetime.utcfromtimestamp(unix_time)
        return datetime.datetime.isoformat(unix_dt)

    def write(self):
        log_line = '{} {}\n'.format(self.datetime, self.body)
        with open(self.filename, 'a+') as logfile:
            logfile.write(log_line)


class LogMsgHandler(tornado.web.RequestHandler):
    """
    Handles log message http requests coming in to the web app's root.

    These log messages are produced by `sitecustomize.py` in the site-packages
    directory of SciTools environments. When the SciTools environment's Python
    is used, the `sitecustomize.py` is run and logs the use, which gets
    recorded to file by this request handler.

    """
    def post(self):
        msg = self.get_argument('msg')
        created = self.get_argument('created')
        # Get file writer from the `settings` dict of the containing
        # tornado `Application` class.
        self.application.settings['writer'](created, msg)


def make_app():
    return tornado.web.Application([(r'/', LogMsgHandler),],
                                   writer=Writer())


if __name__ == '__main__':
    app = make_app()
    app.listen(DEFAULT_HTTP_LOGGING_PORT)
    tornado.ioloop.IOLoop.current().start()

