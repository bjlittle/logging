#!/usr/bin/env bash

# Recreate the conda env that supports the Python invocation logging server running on exvavddoc03.
#
# The conda env is stored in /data/local/avd/miniconda.
#
# The conda env must contain the following libraries:
#   * tornado,
#   * supervisor, and
#   * all of the dependencies of both of these libraries.
#
# This script does the following:
#   1. download miniconda executable to /data/local/avd
#   2. install miniconda into /data/local/avd/miniconda
#   3. remove the downloaded installer
#   4. update all
#   5. install tornado and supervisor.
#
# This script must be run as the avd user.


# Variables.
BASEDIR="/data/local/avd"
DOWNLOAD_NAME="miniconda_latest.sh"
MINICONDA_DIR=${BASEDIR}/miniconda
DOWNLOAD=${BASEDIR}/${DOWNLOAD_NAME}
CONDA=${MINICONDA_DIR}/bin/conda


# Step 1.
wget "https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh" -O ${DOWNLOAD}
chmod u+x ${DOWNLOAD}

# Step 2: install miniconda in batch mode (-b flag).
bash ${DOWNLOAD} -b -p ${MINICONDA_DIR}

# Step 3.
rm ${DOWNLOAD}

# Step 3.
${CONDA} update -y --all

# Step 4.
${CONDA} install -y -n root supervisor tornado
