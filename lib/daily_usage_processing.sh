#!/usr/bin/env bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

# Execute the notebook that records total daily usage of Python over time by Python version.

# Set up executables and input/output files and folder locations.
INPUT_NOTEBOOK=${BASEDIR}/notebook/python_usage_over_time.ipynb
OUTPUT_DIR="/project/avd/python/usage_logs/notebook"
PYTHON="/opt/scitools/environments/default/current/bin/python"
JUPYTER="/opt/scitools/environments/default/current/bin/jupyter"

# Specify executed notebook's name.
OUTPUT_NAME=python_usage_over_time.ipynb

# Capture output to a standard logfile location.
LOG_DIR=${SCRATCH}/logs
mkdir -p ${LOG_DIR}
LOGFILE=${LOG_DIR}/python_daily_usage_execute_notebook.$(date +%Y-%m-%d-%H%M%S).log

# Run the processing script.
unbuffer ${PYTHON} ${BASEDIR}/lib/daily_usage_by_version.py >>${LOGFILE} 2>&1

# Generate the daily usage notebook.
unbuffer ${JUPYTER} nbconvert --to notebook \
                              --allow-errors \
                              --ExecutePreprocessor.timeout=1200 \
                              --output-dir=${OUTPUT_DIR} \
                              --output=${OUTPUT_NAME} \
                              --execute \
                              ${INPUT_NOTEBOOK} >> ${LOGFILE} 2>&1
