#!/usr/bin/env bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

# Set folder locations where the log file starts and needs to end.
# Note the /data/local (localdisk) path - this script should run on
# exvavddoc03 _only_.
LOG_DIR_START="/data/local/avd/logs"
LOG_DIR_END="/project/avd/python/usage_logs"

# Get the name of yesterday's logfile.
PREV_LOGFILE_NAME=$(date --date='yesterday' '+%Y%m%d').txt
PREV_LOGFILE=${LOG_DIR_START}/${PREV_LOGFILE_NAME}

# Capture output to a standard logfile location.
CMD_LOG_DIR=${SCRATCH}/logs
mkdir -p ${CMD_LOG_DIR}
CMD_LOGFILE=${CMD_LOG_DIR}/python_logging_move_logfile.$(date +%Y-%m-%d-%H%M%S).log

# Move the logfile, if it exists.
if [ -e ${PREV_LOGFILE} ]; then
    mv ${PREV_LOGFILE} ${LOG_DIR_END} >> ${CMD_LOGFILE} 2>&1
fi
