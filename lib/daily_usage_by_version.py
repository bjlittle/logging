"""
Find the total number of Python invocations by day by Python version.

Run monthly by a cron job.

"""

import datetime
import fileinput
import os
import glob

import pandas as pd


def last_month_string():
    """
    Determine the filename filter to load only the last month's files (for
    example `201803*.txt`).

    In most cases we only want to read the last month's files to update the
    daily usage file with the latest data.

    """
    now = datetime.datetime.now()
    day_num_now = now.day
    # We want last month's data.
    to_last_month = datetime.timedelta(days=day_num_now)
    last_month = now - to_last_month
    return last_month.strftime('%Y%m*.txt')


class PythonUsageDaily(object):
    def __init__(self, filepath, save_filename, all_files=False):
        self.filepath = filepath
        self.save_filename = save_filename
        self.all_files = all_files

        self.save_file = os.path.join(self.filepath, self.save_filename)
        self.names = ['datetime', 'what', 'sha', 'username',
                      'hostname', 'version', 'exe', 'cmdline', 'pcmdline']

        self._fn_filter = None
        self._filenames = None
        self._results = None
        self._existing_versions = None  # Versions logged in previous months.
        self._month_versions = None  # Versions logged this month.
        self._all_versions = None  # The combination of the two above.
        self._header = None
        self._rewrite_header = None

    @property
    def fn_filter(self):
        if self._fn_filter is None:
            # Set the filename filter if not already set.
            # If the setup specifies we need to load all files, do so.
            if self.all_files:
                # We need this slightly horrible thing to glob-match only
                # filenames made of 8 digits and then '.txt'.
                self.fn_filter = '{}.txt'.format('[0-9]'*8)
            else:
                self.fn_filter = last_month_string()
        return self._fn_filter

    @fn_filter.setter
    def fn_filter(self, value):
        self._fn_filter = value

    @property
    def filenames(self):
        if self._filenames is None:
            self.filenames = glob.glob(os.path.join(self.filepath,
                                                    self.fn_filter))
        return self._filenames

    @filenames.setter
    def filenames(self, value):
        self._filenames = value

    @property
    def results(self):
        if self._results is None:
            self.get_results()
        return self._results

    @results.setter
    def results(self, value):
        self._results = value

    @property
    def existing_versions(self):
        """All unique Python versions logged in previous months."""
        if self._existing_versions is None:
            self._get_existing_versions()
        return self._existing_versions

    @existing_versions.setter
    def existing_versions(self, value):
        self._existing_versions = value

    @property
    def month_versions(self):
        """All unique Python versions logged in the month being processed."""
        if self._month_versions is None:
            self._get_month_versions()
        return self._month_versions

    @month_versions.setter
    def month_versions(self, value):
        self._month_versions = value

    @property
    def all_versions(self):
        """
        All unique Python versions that have been logged in previous months and
        the current month.

        New versions logged in the current month must be appended to the end of
        the previous versions to make `self.all_versions`.

        """
        if self._all_versions is None:
            self._define_all_versions_and_check_header()
        return self._all_versions

    @all_versions.setter
    def all_versions(self, value):
        self._all_versions = value

    @property
    def header(self):
        # Set up the headings line to record the Python versions used.
        if self._header is None:
            self.header = 'datetime,{}\n'.format(','.join(self.all_versions))
        return self._header

    @header.setter
    def header(self, value):
        self._header = value

    @property
    def rewrite_header(self):
        if self._rewrite_header is None:
            self._define_all_versions_and_check_header()
        return self._rewrite_header

    @rewrite_header.setter
    def rewrite_header(self, value):
        self._rewrite_header = value

    def _get_existing_versions(self):
        if not os.path.exists(self.save_file):
            # If the save file doesn't exist we can't rewrite the header!
            # Instead the header will be written in the saver method.
            existing_versions = False
        else:
            # Otherwise we only need to write the header if new versions have
            # been introduced in the month being processed.
            with open(self.save_file, 'r') as osrfh:
                header = osrfh.readlines()[0].rstrip().split(',')
            existing_versions = header[1:]
        self.existing_versions = existing_versions

    def _get_month_versions(self):
        """Get all the Python versions used in the month being processed."""
        mvns = [r[1].keys() for r in self.results]
        # Flatten list of lists and determine the unique results.
        all_month_versions = set([itm for sublist in mvns for itm in sublist])
        self.month_versions = list(all_month_versions)

    def _define_all_versions_and_check_header(self):
        """
        Define the complete set of unique Python versions logged. Exactly the
        same logic defines whether the header needs to be rewritten, so set
        the boolean `self.rewrite_header` here too.

        These will be described by one of three cases:

          1. If there are no existing versions, all versions is given by
             `self.month_versions`.
             Do not rewrite header (there isn't a header to rewrite!)
          2. If no new versions are introduced in the month currently being
             processed, all versions is given by `self.existing_versions`.
             Do not rewrite header (nothing has changed).
          3. If there are existing versions and new versions this month, all
             versions is given by existing versions with the new versions
             appended to the end.
             Do rewrite header (there are new versions to capture).

        """
        if not self.existing_versions:
            # Case 1: no existing versions.
            all_vns = self.month_versions
            rewrite_header = False
        else:
            new_vns = set(self.month_versions) - set(self.existing_versions)
            if not new_vns:
                # Case 2: no new versions.
                all_vns = self.existing_versions
                rewrite_header = False
            else:
                # Case 3: existing versions plus new versions.
                self.existing_versions.extend(list(new_vns))
                all_vns = self.existing_versions
                rewrite_header = True
        self.all_versions = all_vns
        self.rewrite_header = rewrite_header

    def load_and_process(self, filename):
        """
        Load each daily Python usage logfile and process to return a dictionary
        of each Python version present and the number of invocations of that
        version.

        """
        py_usages = pd.read_csv(filename, sep='\s+',
                                names=self.names,
                                engine='python')

        py_usages = py_usages[['what', 'version']]
        startups = py_usages.loc[py_usages['what'] == 'startup']
        versions = pd.unique(startups['version'])
        counts = startups.groupby('version').count()
        return {version: counts.loc[version]['what'] for version in versions}

    def get_results(self):
        """
        Process all files and get the Python usages by version from each file.

        """
        results = []
        for filename in self.filenames:
            print(filename)
            datetime = filename.split(os.sep)[-1].split('.')[0]
            contents = self.load_and_process(filename)
            results.append([datetime, contents])
        self.results = results

    def process_results(self):
        """Check the results are contiguous etc."""
        for result in self.results:
            versions = set(result[1].keys())
            version_diff = list(set(self.month_versions) - versions)
            if len(version_diff):
                for diff in version_diff:
                    result[1][diff] = 0

    def _update_header(self):
        """
        Update the daily usages file header if new Python versions have been
        used in the month being processed, so that the header also records
        these new Python versions.

        """
        if self.rewrite_header:
            # New Python version(s) have been introduced, which need to be
            # captured in the header, so replace the file's header line.
            daily_uses_file = fileinput.input(self.save_file, inplace=True)
            for line in daily_uses_file:
                # In this context, `print` writes the printed content to the
                # line in the file.
                if daily_uses_file.isfirstline():
                    print(self.header.rstrip())
                else:
                    print(line.rstrip())
            daily_uses_file.close()

    def saver(self):
        """
        Write the month's Python usage by version to `self.save_file`. If new
        Python versions have been used in this month, also updates the daily
        usages file's header to record the new versions.

        """
        # Check if the daily usages file header needs to be replaced.
        self._update_header()
        # Now write content to the file.
        if not os.path.exists(self.save_file):
            write_header = True
        else:
            write_header = False
        with open(self.save_file, 'a') as osfh:
            if write_header:
                # New file, so we need to add the header line to the file.
                osfh.write(self.header)
            for result in self.results:
                datetime, counts = result
                vals = [str(counts.get(version, 0))
                        for version in self.all_versions]
                usages = ','.join(vals)
                line = '{},{}\n'.format(datetime, usages)
                osfh.write(line)

    def runner(self):
        """Interface: run all processing."""
        self.process_results()
        self.saver()


if __name__ == '__main__':
    filepath = '/project/avd/python/usage_logs/'
    save_filename = 'daily_usage.txt'
    processor = PythonUsageDaily(filepath, save_filename)
    processor.process_results()
    processor.saver()
