#!/usr/bin/env bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

# Specify input location of template notebook, and output location of executed notebook.
INPUT_NOTEBOOK=${BASEDIR}/notebook/logfile_postprocessing.ipynb
OUTPUT_DIR="/project/avd/python/usage_logs/notebook"
JUPYTER="/opt/scitools/environments/default_legacy/current/bin/jupyter"

# Specify executed notebook's name.
YESTERDAY=$(date --date='yesterday' '+%Y%m%d')
OUTPUT_NAME=logfile_postprocessing_${YESTERDAY}.ipynb

# Capture output to a standard logfile location.
LOG_DIR=${SCRATCH}/logs
mkdir -p ${LOG_DIR}
LOGFILE=${LOG_DIR}/python_logging_execute_notebook.$(date +%Y-%m-%d-%H%M%S).log

# Execute the notebook.
unbuffer ${JUPYTER} nbconvert --to notebook \
                              --allow-errors \
                              --ExecutePreprocessor.timeout=300 \
                              --output-dir=${OUTPUT_DIR} \
                              --output=${OUTPUT_NAME} \
                              --execute \
                              ${INPUT_NOTEBOOK} >> ${LOGFILE} 2>&1
