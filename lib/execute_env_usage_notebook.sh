#!/usr/bin/env bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

# Execute the notebook that records SSS env usage by user.

# Set up executables and input/output files and folder locations.
INPUT_NOTEBOOK=${BASEDIR}/notebook/SSS_env_usage.ipynb
OUTPUT_DIR="/project/avd/python/usage_logs/notebook"
PYTHON="/opt/scitools/environments/default/current/bin/python"
JUPYTER="/opt/scitools/environments/default/current/bin/jupyter"

# Specify executed notebook's name.
OUTPUT_NAME=SSS_env_usage.ipynb

# Capture output to a standard logfile location.
LOG_DIR=${SCRATCH}/logs
mkdir -p ${LOG_DIR}
LOGFILE=${LOG_DIR}/sss_env_usage_execute_notebook.$(date +%Y-%m-%d-%H%M%S).log

# Execute the SSS env usage notebook. This contains code that updates its plots and listings from the latest logfile data
unbuffer ${JUPYTER} nbconvert --to notebook \
                              --allow-errors \
                              --ExecutePreprocessor.timeout=900 \
                              --output-dir=${OUTPUT_DIR} \
                              --output=${OUTPUT_NAME} \
                              --execute \
                              ${INPUT_NOTEBOOK} >> ${LOGFILE} 2>&1
