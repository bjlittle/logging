# Python Invocation Logging

These files log the usage of Python and its libraries for all users of the
Scientific Software Stack by creating a log record at the start and end of each
SciTools Python invocation.

Logging of Python invocations can be disabled on a per-session basis by setting
the environment variable ```DISABLE_PY_LOGGING```.  For example:

    $ export DISABLE_PY_LOGGING=True
    $ python

## Components used for logging

The logging process is split into modules with separate tasks:

### 1. ```sitecustomize.py```

This script is part of the ```mo_sitecustomize``` library, which resides in the
[mo_sitcustomize repository](https://exxgitrepo:8443/projects/AVD/repos/mo_sitecustomize/browse?at=refs%2Fheads%2Fmaster).
This repository is included in all SciTools environments.  It creates a log record
of each Python invocation on the Stack using Python's logging module.  This
includes multiple invocations by the same user at the same time, each logged as
a separate record.

__Note:__
> Each session creates two log entries; one at startup and another at shutdown.  They
> are matched up for analysis by a session-specific sha which is created
> in this module.


### 2. ```python_logging_app.py```

This app listens for log entries created by ```sitecustomize.py```,
then sorts and writes the details into a log file.

__Note:__
> The python_logging_app.py needs to be running constantly to gather Python usage
> data, so it is handled by the Python library [```supervisor```](supervisord.org).
> [This config file](https://exxgitrepo:8443/projects/AVD/repos/crons/browse/supervisor/python_logging.prog.conf)
> tells ```supervisor``` to run the logging server as a daemon process and restart it
> automatically if it stops running for any reason.
> 
> The running of ```supervisor``` is controlled by the [crons repo](https://exxgitrepo:8443/projects/AVD/repos/crons).


### 3. Analysis notebooks

These can be used to translate the noisy log files into a form which can be
easily interpreted by a human, including graphical representations of the data
over various timescales.
A [template notebook](https://exxgitrepo:8443/projects/SSS/repos/logging/browse/notebook/logfile_postprocessing.ipynb?at=refs%2Fheads%2Fmaster)
is executed daily with each new set of usage data. The resultant notebook can be
found in this location: ```/project/avd/python/usage_logs/notebooks```.

__Note:__ 
> Crontabs related to the Python usage logging process
> [can be found here](https://exxgitrepo:8443/projects/AVD/repos/crons/browse/crontabs/exvavddoc03.avd.crontab).
> These cron jobs restart ```supervisord``` upon reboot of ```exvavddoc03```
> and move the logfiles to an archive location on a daily basis.
> 
> Crontabs to a) produce the analysis notebook from daily Python invocation data (daily) and 
> b) get daily total Python usage by version for the previous month and update the daily usage 
> notebook (monthly) [can be found here](https://exxgitrepo:8443/projects/AVD/repos/crons/browse/crontabs/els056.avd.crontab)
>

Any of these crontabs can be run immediately (rather than waiting for their next scheduled)
execution. This may be useful (for example) for ensuring a newly deployed notebook runs correctly and updates
itself immediately. To do this ssh (as avd) to the relevant box where the crontabs are 
based (e.g. els056 for those mentioned in the second 'note' paragraph above). Then edit the crontab 
with 'crontab -e' and change the crontab to run in a few minutes' time 
(see e.g. [https://crontab.guru/](https://crontab.guru/) for the syntax). Note that cronanny will
automatically revert these short term changes to crontab schules when it runs at ten and forty minutes past 
each hour.


## Locations of logfiles

| Logfile | Location |
| --- | --- |
| In-progress Python invocation logs | ```exvavddoc03:/data/local/avd/logs``` |
| Archived Python invocation logs | ```/project/avd/python/usage_logs``` |
| Cron logs | ```/scratch/avd/logs/python_invoc_logging.<date>.log``` |
| Supervisor-restart shell script logs | ```/scratch/avd/logs/python_logserver_restart.<date>.log``` |
| Supervisor logs | ```/scratch/avd/logs/supervisord.log``` |

