{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Processing Python Usage Logfiles \n",
    "\n",
    "This notebook processes yesterday's Python usage logfile and produces a series of useful graphs summarising yesterday's Python usage. It is designed to be executed automatically and then viewed statically, but there is no reason why you shouldn't also interact with it directly; for example in order to add custom analysis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "\n",
    "* [Setup](#Setup)\n",
    "* [Load and tidy data](#Load-and-tidy-data)\n",
    "    * [Split dataframe: startup and shutdown](#Split-dataframe:-startup-and-shutdown)\n",
    "    * [Shutdown dataframe processing](#Shutdown-dataframe-processing)\n",
    "    * [Startup dataframe processing](#Startup-dataframe-processing)\n",
    "    * [Recombine](#Recombine)\n",
    "    * [Adding value](#Adding-value)\n",
    "* [Analysis and Plotting](#Analysis-and-Plotting)\n",
    "    * [Counting usages](#Counting-usages)\n",
    "        * [Count by username](#Count-by-username)\n",
    "            * [Count users of classic-2.7.6](#Count-users-of-classic-2.7.6)\n",
    "        * [Count by hostname](#Count-by-hostname)\n",
    "        * [Count by Python version](#Count-by-Python-version)\n",
    "        * [Count by Python executable](#Count-by-Python-executable)\n",
    "    * [Usage over time](#Usage-over-time)\n",
    "        * [Dataframe setup](#Dataframe-setup)\n",
    "        * [Top 10 users](#Top-10-users)\n",
    "        * [Usage by Platform](#Usage-by-Platform)\n",
    "        * [Python versions](#Python-versions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup\n",
    "\n",
    "Import necessary libraries and set pandas display options."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import datetime\n",
    "import os\n",
    "\n",
    "from matplotlib.colors import ListedColormap\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.set_option('display.max_rows', 16)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load and tidy data\n",
    "\n",
    "Yesterday's logfile is loaded and processed using pandas. Each step in the loading and processing has a light description associated with it that outlines what the step is doing and why it is beneficial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the name of yesterday's logfile. The logfile will have name of the form 'yyyymmdd.txt'.\n",
    "today = datetime.date.today()\n",
    "datedelta = datetime.timedelta(days=1)\n",
    "yesterday = (today - datedelta).strftime('%Y%m%d')\n",
    "print yesterday"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "logfile_archive_filepath = '/project/avd/python/usage_logs/'\n",
    "filename = '{}.txt'.format(yesterday)\n",
    "logfile = os.path.join(logfile_archive_filepath, filename)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "py_usages = pd.read_table(logfile, sep='\\s+',\n",
    "                          parse_dates=[0],\n",
    "                          names=['datetime', 'what', 'sha', 'username', 'hostname', 'version', 'exe', 'cmdline', 'pcmdline'],\n",
    "                          usecols=['datetime', 'what', 'sha', 'username', 'hostname', 'version', 'exe'],\n",
    "                          engine='python')\n",
    "py_usages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Split dataframe: startup and shutdown\n",
    "\n",
    "Split the dataframe into one dataframe containing Python invocation (startup) logs and one containing Python shutdown logs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "startups = py_usages.loc[py_usages['what'] == 'startup']\n",
    "startups"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "shutdowns = py_usages.loc[py_usages['what'] == 'shutdown']\n",
    "shutdowns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Shutdown dataframe processing\n",
    "\n",
    "The `shutdowns` dataframe contains a number of columns that we do not need (including the \"shutdown\" column, which we've now split the original dataframe with), as well as an unhelpful column containing all the imports made during the Python invocation. Let's tidy these things up.\n",
    "\n",
    "#### Remove extraneous columns\n",
    "\n",
    "Most of the columns in the `shutdowns` dataframe don't contain any data, so we can remove them. We can also rename a couple of columns to make them more descriptive."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "shutdowns = shutdowns.drop(['what', 'hostname', 'version', 'version', 'exe'], axis=1)\n",
    "shutdowns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "shutdowns = shutdowns.rename(columns={'datetime': 'end', 'username': 'imports'})\n",
    "shutdowns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Tidy imports\n",
    "\n",
    "There are many libraries imported in every Python session, so let's filter all these imports to a small number of library imports that we _are_ interested in."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "libs_of_interest = set(['biggus', 'cartopy', 'cf_units', 'iris', 'IPython', 'jupyter_core',\n",
    "                        'matplotlib', 'numpy', 'scipy'])\n",
    "\n",
    "def imports_filter(imports):\n",
    "    imports_list = imports.split(',')\n",
    "    invoc_imports = set(imports_list)\n",
    "    common = invoc_imports & libs_of_interest\n",
    "    if len(common) == 1:\n",
    "        result, = list(common)\n",
    "    elif len(common):\n",
    "        result = list(common)\n",
    "    else:\n",
    "        result = None\n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "shutdowns['imports'] = shutdowns['imports'].apply(imports_filter)\n",
    "shutdowns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Startup dataframe processing\n",
    "\n",
    "#### Column names\n",
    "\n",
    "The dataframe containing startup information requires less processing. Again though, we can remove the \"up_down\" column as we've split the original dataframe using the contents of this column.\n",
    "\n",
    "Let's also take this opportunity to rename the \"date_time\" column, which was shared with the `shutdowns` dataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "startups = startups.drop('what', axis=1)\n",
    "startups"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "startups = startups.rename(columns={'datetime': 'start'})\n",
    "startups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Tidy executable locations\n",
    "\n",
    "The executable locations are a bit unwieldy, so let's replace them with more descriptive names where possible. We'll do this by running an `apply` function to the `exe` column."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Determine tagged and labelled envs\n",
    "\n",
    "Determine all stack envs currently deployed, both by tag (e.g. `2017_06_07`) and label (e.g. `default_current`), as a stack env could be accessed either via its tag or label. These are used to improve the labelling (and so reporting) of levels of usage of each stack env.\n",
    "\n",
    "Note: this does not currently include all tags deployed on the HPC."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "basedir = '/opt/scitools/environments'\n",
    "suffix = 'bin/python'\n",
    "names = ['default', 'default_legacy', 'experimental', 'experimental_legacy', 'production', 'production_legacy']\n",
    "# We won't use this bit when comparing paths.\n",
    "cutoff_len = len('/opt/scitools/environments/')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Find all labelled envs (on the desktop/SPICE).\n",
    "# Labelled envs are always symlinks to tagged releases of the stack.\n",
    "\n",
    "labels = []\n",
    "for name in names:\n",
    "    name_dir = os.path.join(basedir, name)\n",
    "    # `os.islink` does not work in notebooks...\n",
    "    symlinks = !find $name_dir -maxdepth 1 -type l\n",
    "    labels.extend(symlinks)\n",
    "    \n",
    "labels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make a lookup dictionary linking symlinks (labels) to full directories (tags).\n",
    "# As the full paths may vary across platforms, we shorten paths to only consider\n",
    "# the common parts of the paths (effectively the last 4 bits in the path).\n",
    "\n",
    "label_tag_lookup = {k[cutoff_len:]: os.path.join(os.readlink(k), suffix)[cutoff_len:]\n",
    "                    for k in labels}\n",
    "label_tag_lookup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make a lookup dictionary linking tags to human-readable names.\n",
    "# Note: the old Python 2.7.6 version is added manually.\n",
    "\n",
    "tag_name_lookup = {v: k.replace('/', '_') for k, v in label_tag_lookup.items()}\n",
    "tag_name_lookup['/usr/local/sci/bin/python'] = 'classic-2.7.6'\n",
    "tag_name_lookup['/opt/python/gnu/2.7.9/bin/python'] = 'opt-2.7.9'\n",
    "tag_name_lookup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def human_env_names(exe):\n",
    "    # A fallback value if the exe could not be named.\n",
    "    exe_name = exe\n",
    "    # First check if the exe is a known symlink (label).\n",
    "    for label, tag in label_tag_lookup.items():\n",
    "        if label in exe:\n",
    "            exe = tag\n",
    "    # Now check if the exe is a known tag directory.\n",
    "    for tag, name in tag_name_lookup.items():\n",
    "        if tag in exe:\n",
    "            exe_name = name\n",
    "    return exe_name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Apply to dataframe\n",
    "\n",
    "With available names now determined, we can apply renaming of the executables to the dataframe of startup usages:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "startups['exe'] = startups['exe'].apply(human_env_names)\n",
    "startups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Recombine\n",
    "\n",
    "Now that we've split and tidied the log file data, we can recombine the data by **joining** on the SHA column, to produce one row for an entire Python invocation; startup --> shutdown."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "invocs_df = startups.merge(shutdowns, on='sha')\n",
    "invocs_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Tidying\n",
    "\n",
    "We no longer need the sha column, so we can drop that. It would be nice to rearrange the order of the columns a little too, so let's do that as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "invocs_df = invocs_df.drop('sha', axis=1)\n",
    "invocs_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cols = invocs_df.columns\n",
    "new_cols = [cols[0], cols[-2], cols[1], cols[2], cols[3], cols[4], cols[-1]]\n",
    "invocs_df = invocs_df[new_cols]\n",
    "invocs_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adding value\n",
    "\n",
    "With a tidied and combined dataframe we can add some value to the data we have. In particular, let's:\n",
    "\n",
    "* add a new column that gives the session length, found through the difference `end - start`.\n",
    "* add a new column that makes a guess at the invocation method. We don't have a proper metric for this as we can't log it, but we can use the imports of the libraries `IPython` and `jupyter_core` as an indication that IPython and jupyter, respectively, started this invocation.\n",
    "\n",
    "#### Session lengths column"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "invocs_df['session_length'] = invocs_df['end'] - invocs_df['start']\n",
    "invocs_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Guess invocation method\n",
    "\n",
    "We can make a rough guess at invocation method from the imports:\n",
    "\n",
    "* If `jupyter_core` is imported, we can guess that a jupyter notebook was the invocation method.\n",
    "* If `IPython` was imported but _not_ `jupyter_core` (as this also imports `IPython`), we can guess that an IPython session was the invocation method.\n",
    "* Otherwise, we just don't know! We'll say it was a plain Python session, but we can't say whether this session was executing a module or running as a terminal session."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def guess_invoc_method(imports):\n",
    "    \"\"\"\n",
    "    Make a guess at the invocation method.\n",
    "    \n",
    "    The logic for this guess is laid out in the text cell above.\n",
    "    \n",
    "    \"\"\"\n",
    "    if isinstance(imports, basestring) or imports is None:\n",
    "        imports = [imports]\n",
    "    if 'jupyter_core' in imports:\n",
    "        invoc_type = 'jupyter'\n",
    "    elif 'IPython' in imports:\n",
    "        invoc_type = 'IPython'\n",
    "    else:\n",
    "        invoc_type = 'python'\n",
    "    return invoc_type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "invocs_df['invoc_type'] = invocs_df['imports'].apply(guess_invoc_method)\n",
    "invocs_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analysis and Plotting\n",
    "\n",
    "Let's analyse our usage logs and produce plots of what we find out."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a standard figure size.\n",
    "plot_figsize = (20, 12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Counting usages\n",
    "\n",
    "First up: counting usages by user, host, Python version and Python executable.\n",
    "\n",
    "#### Count by username"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "user_count = invocs_df.groupby('username').count().sort_values('version', ascending=False)\n",
    "user_count"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "top_10_users = list(user_count.index.values)[:10]\n",
    "top_10_users"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "version_count = user_count['version']\n",
    "version_count[version_count > 100].plot(kind='bar', figsize=plot_figsize)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "host_count = invocs_df.groupby('hostname').count().sort_values('version', ascending=False)\n",
    "host_count"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "version_count[version_count > 100].plot(kind='bar', figsize=plot_figsize, logy=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Count users of `classic-2.7.6`\n",
    "\n",
    "We are particularly interested in heavy users of Python 2.7.6 from `/usr/local/sci`, as this version of Python will not be available in RHEL7."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "classic_276_users = invocs_df[invocs_df['exe'] == 'classic-2.7.6']\n",
    "classic_276_users_count = classic_276_users.groupby('username').count().sort_values('version', ascending=False)\n",
    "classic_276_users_count"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now show all the users of Python 2.7.6 from /usr/local/sci ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with pd.option_context('display.max_rows', 500):\n",
    "    print classic_276_users_count['version']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Count by hostname"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "host_count = invocs_df.groupby('hostname').count().sort_values('version', ascending=False)\n",
    "host_count"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "top_25_hosts = list(host_count.index.values)[:25]\n",
    "top_25_hosts"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "host_count['version'][:25].plot(kind='bar', figsize=plot_figsize)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Count by Python version"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "version_count = invocs_df.groupby('version').count().sort_values('exe', ascending=False)\n",
    "version_count"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "version_count['exe'].plot(kind='bar', figsize=plot_figsize)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "version_count['exe'].plot(kind='bar', figsize=plot_figsize, logy=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Count by Python executable"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exe_count = invocs_df.groupby('exe').count().sort_values('version', ascending=False)\n",
    "exe_count"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "exe_count['version'].plot(kind='bar', figsize=plot_figsize)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "exe_count['version'].plot(kind='bar', figsize=plot_figsize, logy=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Usage over time\n",
    "\n",
    "Let's also look at how Python usage varies hour-by-hour. To do that we need to \"resample\" our dataframe to hold hourly data.\n",
    "\n",
    "As it happens, resampling does not quite do what we want it to in this case, so instead we achieve some poor man's binning by throwing away sub-hour data from our datetimes, again by running an apply against one of the dataframe's columns. This means we need to reset our dataframe's index so that we can access the datetime column."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Specialised colormap\n",
    "\n",
    "This analysis will produce a series of line plots with up to 10 discrete lines in each plot. To improve legibility of these plots, we define a colormap containing only 10 discrete colours, produced from an existing matplotlib colormap."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "color_list = plt.cm.Paired(np.linspace(0, 1, 12))\n",
    "cm_seg_12 = ListedColormap(color_list, 'seg10', 12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Dataframe setup\n",
    "\n",
    "To produce plots of usage over time we first need to process the dataframe a little further so that it contains the existing data in the format that we need for producing these plots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "invocs_df_noindex = invocs_df.reset_index()\n",
    "invocs_df_noindex"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "invocs_df_noindex['start'] = invocs_df_noindex['start'].apply(lambda dt: dt.strftime('%Y-%m-%d %H'))\n",
    "invocs_df_noindex"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "invocs_df_noindex_dt_user = invocs_df_noindex.groupby(['start', 'username']).count().sort_values('index', ascending=False)\n",
    "invocs_df_noindex_dt_user"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plottable_dt_user = invocs_df_noindex_dt_user['version'].unstack(level=1)\n",
    "plottable_dt_user"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Top 10 users"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "plottable_dt_user[top_10_users].plot(figsize=plot_figsize, colormap=cm_seg_12)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "plottable_dt_user[top_10_users].plot(figsize=plot_figsize, logy=True, colormap=cm_seg_12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Usage by Platform"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_platform(hostname):\n",
    "    if 'eld' in hostname:\n",
    "        result = 'Desktop'\n",
    "    elif 'exx' in hostname:\n",
    "        result = 'Desktop'\n",
    "    elif 'elc' in hostname:\n",
    "        result = 'Desktop'\n",
    "    elif 'rld' in hostname:\n",
    "        result = 'Desktop'\n",
    "    elif 'vld' in hostname:\n",
    "        result = 'Desktop'\n",
    "    elif 'els' in hostname:\n",
    "        result = 'Server'\n",
    "    elif 'cls' in hostname:\n",
    "        result = 'Server'\n",
    "    elif 'exv' in hostname:\n",
    "        result = 'Server'\n",
    "    elif 'metoffice' in hostname:\n",
    "        result = 'Server'\n",
    "    elif 'spice' in hostname:\n",
    "        result = 'SPICE'\n",
    "    elif 'cylc' in hostname:\n",
    "        result = 'cylc'\n",
    "    elif 'xce' in hostname:\n",
    "        result = 'XCE'\n",
    "    elif 'xcf' in hostname:\n",
    "        result = 'XCF'\n",
    "    elif 'xcs' in hostname:\n",
    "        result = 'XCS'\n",
    "    elif 'shared' in hostname:\n",
    "        result = 'XCS'\n",
    "    elif 'nid' in hostname:\n",
    "        result = 'XCS'\n",
    "    elif 'mom' in hostname:\n",
    "        result = 'mom'\n",
    "    else:\n",
    "        result = hostname\n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "invocs_df_noindex['hostname'] = invocs_df_noindex['hostname'].apply(get_platform)\n",
    "invocs_df_noindex"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "invocs_df_noindex_dt_host = invocs_df_noindex.groupby(['start', 'hostname']).count().sort_values('index', ascending=False)\n",
    "invocs_df_noindex_dt_host"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plottable_dt_host = invocs_df_noindex_dt_host['version'].unstack(level=1)\n",
    "plottable_dt_host"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plottable_dt_host.plot(figsize=plot_figsize, colormap=cm_seg_12)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plottable_dt_host.plot(figsize=plot_figsize, logy=True, colormap=cm_seg_12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Python versions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dp = invocs_df_noindex['exe'] == 'default_previous'\n",
    "dc = invocs_df_noindex['exe'] == 'default_current'\n",
    "dn = invocs_df_noindex['exe'] == 'default_next'\n",
    "dlp = invocs_df_noindex['exe'] == 'default_legacy_previous'\n",
    "dlc = invocs_df_noindex['exe'] == 'default_legacy_current'\n",
    "dln = invocs_df_noindex['exe'] == 'default_legacy_next'\n",
    "po39 = invocs_df_noindex['exe'] == 'production_os39'\n",
    "po40 = invocs_df_noindex['exe'] == 'production_os40-1'\n",
    "po41 = invocs_df_noindex['exe'] == 'production_os41-1'\n",
    "pol42 = invocs_df_noindex['exe'] == 'production_legacy-os42-1'\n",
    "po42 = invocs_df_noindex['exe'] == 'production-os42-1'\n",
    "ec = invocs_df_noindex['exe'] == 'experimental_current'\n",
    "elc = invocs_df_noindex['exe'] == 'experimental_legacy_current'\n",
    "cl = invocs_df_noindex['exe'] == 'classic-2.7.6'\n",
    "op = invocs_df_noindex['exe'] == 'opt-2.7.9'\n",
    "\n",
    "invocs_df_noindex_major_exe = invocs_df_noindex[dp | dc | dn | dlp | dlc | dln | po39 | po40 | po41 | pol42 | po42 | ec | elc | cl | op]\n",
    "invocs_df_noindex_major_exe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "invocs_df_noindex_dt_exe = invocs_df_noindex_major_exe.groupby(['start', 'exe']).count()\n",
    "invocs_df_noindex_dt_exe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plottable_dt_exe = invocs_df_noindex_dt_exe['version'].unstack(level=1).fillna(0)\n",
    "plottable_dt_exe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "plottable_dt_exe.plot(figsize=plot_figsize, colormap=cm_seg_12)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "plottable_dt_exe.plot(figsize=plot_figsize, logy=True, colormap=cm_seg_12)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.15"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
